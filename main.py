import numpy as np
from matplotlib import pyplot as plt
from skimage.measure import label
import cv2

img = cv2.imread("balls_and_rects.png")
im_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
th, im_gray_th_otsu = cv2.threshold(im_gray, 128, 192, cv2.THRESH_OTSU)
labeled = label(im_gray_th_otsu)

results = {}
circles = 0
rects = 0

for i in range(1, np.max(labeled) + 1):
    wh = np.where(labeled == i)
    y_min = wh[0][0]
    x_min = wh[1][0]
    y_max = wh[0][-1]
    x_max = wh[1][-1]
    if (x_max - x_min + 1) * (y_max - y_min + 1) == len(wh[0]):
        f = 'r'
    else:
        f = 'c'
    hue = img[y_min][x_min][0]
    if hue not in results:
        results[hue] = [0, 0]
    if f == 'r':
        results[hue][0] += 1
        rects += 1
    else:
        results[hue][1] += 1
        circles += 1

print(f"Total figures: {np.max(labeled)}")
print(f"Total hues: {len(results)}")
print(f"Totals rects: {rects}, circles: {circles}")
for result in results:
    print(f"{result}: rects: {results[result][0]}, circles: {results[result][1]}")
